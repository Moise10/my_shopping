import React,{useState} from 'react';
import s from './wig.scss'
import CssModules from 'react-css-modules'
import { Button } from 'reactstrap';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';

const Wigs = ({history}) => {
  const [dropdownOpen, setOpen] = useState(false);

  const toggle = () => setOpen(!dropdownOpen);

  return ( 
    <div className='wigs'>
    <ButtonDropdown  isOpen={dropdownOpen} toggle={toggle} >
    <Button id="caret" color="primary" className='Button'
    style={{color:'black', fontSize: '19px'}}
    onClick={ () => history.push('/collection/frontal_wigs')} >
      Lace wigs
    </Button>
    <DropdownToggle caret color="none" border='none' 
    />
    <DropdownMenu >
      <DropdownItem onClick={ () => history.push('/collection/lace_wig')}>
        13 x 6 Lace frontal wig
      </DropdownItem>
      <DropdownItem onClick={ () => history.push('/collection/lace_frontal_wig')}>
        Frontal wig
      </DropdownItem>
      <DropdownItem onClick={ () => history.push('/collection/full_lace_wig')}>
        360 Lace frontal wig
      </DropdownItem>
    </DropdownMenu>
  </ButtonDropdown>
    </div>
  
  );
}
 
export default withRouter(CssModules(Wigs,s));