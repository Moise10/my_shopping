import React from 'react';
import s from './custom-input.scss'
import CssModules from 'react-css-modules'


const CustomInput = ({handleChange, label, ...otherProps}) => {
  return ( 
    <div className='group'>
      <input className='form-input' {...otherProps} onChange={handleChange}/>
      {
        label ? 
        (<label className= {`${otherProps.value.length ? 'shrink' : '' } form-input-label`}>{label}</label>) : null
      }
    </div>
    );
}
 
export default CssModules(CustomInput,s);