import React from 'react';
import SignIn from '../sign-in/sign-in';
import s from './sign-in-sign-up.scss'
import CssModules from 'react-css-modules'
import SignUp from '../sign-up/sign-up';
import Directory from '../directory/directory'


const SignInSignUp = () => {
  return ( 
    <div>
      <div className="directory">
        <Directory />
      </div>
    <div className='sign-in-sign-up'>
      <SignIn />
      <SignUp />
    </div>
    </div>
    
  );
}


export default CssModules(SignInSignUp,s);
