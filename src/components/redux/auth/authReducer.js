
const INITIAL_STATE = {
  authError: null,
}

const authReducer = (state = INITIAL_STATE, action) => {
  switch(action.type){
    case "LOGIN_SUCCESS":
      console.log('login success')
      return {
        ...state,
        authError: null
      };
    case "LOGIN_ERROR":
      console.log('login failed')
      return {
        ...state,
        authError: 'login failed' 
      };
    case "SIGN_OUT":
      console.log('logout success')
      return {
        ...state,
      };
    case "SIGNUP_SUCCESS":
      console.log('signup success')
      return {
        ...state,
        authError: null
      };
    case "SIGNUP_ERROR":
      return {
        ...state,
        authError: action.error.message
      }
      default:
        return state
  }
}

export default authReducer;