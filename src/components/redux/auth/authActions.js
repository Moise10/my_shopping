export const signInAuth = (credentials) => {
  return (dispatch, getState, {getFirebase}) => {
    const firebase = getFirebase();
    firebase.auth().signInWithEmailAndPassword(
      credentials.email,
      credentials.password
    ).then(() => dispatch({
      type: 'LOGIN_SUCCESS'
    })).catch((e) => dispatch({type: 'LOGIN_ERROR', e}))
  }
}

export const signOutAuth = () => {
  return (dispatch, getState, {getFirebase}) => {
    const firebase = getFirebase();
    firebase.auth().signOut().then(() => dispatch({
      type: "SIGN_OUT"
    }))
  }
}


export const signUp = (newUser) => {
  return (dispatch, getState, {getFirebase, getFirestore}) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firebase.auth().createUserWithEmailAndPassword(
      newUser.email,
      newUser.password
    ).then((resp) => {
      return firestore.collection('users').doc(resp.user.uid).set({
        email: newUser.email,
        name: newUser.name,
        password: newUser.password,
        confirmPassword: newUser.confirmPassword
      })
    }).then(() => dispatch({
      type: "SIGNUP_SUCCESS"
    })).catch((error) => dispatch({
      type: 'SIGNUP_ERROR',error
    }))
  }
}