import { createSelector } from 'reselect';


const selectFirebase = state => state.firebase.auth;

export const uid = createSelector(
  [selectFirebase],
  auth => auth.uid
)
