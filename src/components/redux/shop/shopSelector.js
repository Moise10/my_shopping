import {createSelector} from 'reselect'


export const selectShop = state => state.shop;

export const selectCollections = createSelector(
  [selectShop],
  shop =>  shop ? shop.collections : null
)

export const selectProducts = product => createSelector(
  [selectCollections],
  collections => collections ? collections.products : []
)

export const convertCollectionsToArray = createSelector(
[selectCollections],
collections => collections ? Object.keys(collections).map(key => collections[key]) : []
)

export const selectCollection = collectionUrlParams =>createSelector(
  [selectCollections],
  collections => (collections ?  collections[collectionUrlParams] : null)
)

export const selectCollectionIsFetching = createSelector(
  [selectShop],
  shop => shop.isFetching
)

export const selectIsCollectionLoaded = createSelector(
  [selectShop],
  shop => !!shop.collections
)