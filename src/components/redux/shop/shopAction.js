import { shopActionTypes } from './shop.types';
import {firestore} from '../../firebase/firebase.utils'


export const fetchCollectionsStart = () => ({
  type: shopActionTypes.FETCH_COLLECTIONS_START
})

export const fetchCollectionsSuccess = (collectionMap) => ({
  type: shopActionTypes.FETCH_COLLECTIONS_SUCCESS,
  payload: collectionMap
})

export const fetchCollectionsFailure = (error) => ({
  type: shopActionTypes.FETCH_COLLECTIONS_FAILURE,
  payload: error
})

export const fetchCollectionsAsync = () => {
  return (dispatch) => {
    const collectionRef = firestore.collection("collections");
    dispatch(fetchCollectionsStart())
    collectionRef.get().then((snapshot) => {
      const convertCollectionsSnapshot = collections => {
        const transformedCollection = collections.docs.map(doc => {
          const {title, products} = doc.data();
          return {
            linkUrl: encodeURI(title.toLowerCase()),
            id: doc.id,
            products,
            title
          }
        })
        return transformedCollection.reduce((accumulator, collection) => {
          accumulator[collection.title.toLowerCase()] = collection;
          return accumulator
        })
      }
      const collectionMap = convertCollectionsSnapshot(snapshot)
      dispatch(fetchCollectionsSuccess(collectionMap))
    }).catch((e) => dispatch(fetchCollectionsFailure(e)))
  }
}