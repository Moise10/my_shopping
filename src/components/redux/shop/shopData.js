export const shop_data = 
{
  shop_hair: {
    id: 1,
    route: 'virgin_hair',
    title: 'shop_hair',
    items: [
        {
          id: 40,
          texture:['straight','Body wave', 'deep wave'],
          size: ['10','12','14', '16', '18', '20','22', ''],
          description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. ',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          price: 54,
          images:[''],
          inStock: false,
          name: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, quidem ',
          category: ['virgin hair']
        },
        {
          id: 41,
          texture:['straight','Body wave'],
          size: ['10','12','14', '16', '18', '20','22', ''],
          description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. ',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          price: 54,
          images:[''],
          inStock: true,
          name: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, quidem ',
          category: ['virgin hair']
        },
        {
          id: 42,
          texture:['straight','Body wave', 'deep wave'],
          size: ['10','12','14', '16', '18', '20','22', ''],
          description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. ',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          price: 54,
          images:[''],
          inStock: false,
          name: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, quidem ',
          category: ['ombre hair']
        },
          {
          id: 43,
          texture:['straight','Body wave', 'deep wave'],
          size: ['10','12','14', '16', '18', '20','22', ''],
          description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. ',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          price: 54,
          images:[''],
          inStock: false,
          name: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, quidem ',
          length_of_closure: ['10', '12', '14', '16', '18'],
          length_of_hair: ['10 12 14', '20 12 26', '18 16 28'],
          category: ['blonde hair with closure']
          },
          {
          id: 44,
          texture:['straight', 'deep wave'],
          size: ['10','12','14', '16', '18'],
          description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. ',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          price: 54.45,
          images:[''],
          inStock: true,
          name: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, quidem ',
          category: ['closure frontal']
        }
      ]
      },
  lace_wig:{
    id: 2,
    route: 'lace_wig',
    title: 'lace wigs',
    items: [
          {
          id: 60,
          size: ['8', '10', '12', '14', '20'],
          price: 23.43,
          name:  'quibusdam natus ab reiciendis quia, cum laudantium tenetur alias maiores quisquam a.',
          description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          instock: true,
          category: ['BOB']
        },
        {
          id: 61,
          size: [ '14', '20', '22', '24', '28'],
          price: 63.43,
          name:  'quibusdam natus ab reiciendis quia, cum laudantium tenetur alias maiores quisquam a.',
          description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          instock: true,
          category: ['BOB']
        },
        {
          id: 62,
          size: ['8', '10', '20', '22', '24'],
          price: 26.43,
          name:  'quibusdam natus ab reiciendis quia, cum laudantium tenetur alias maiores quisquam a.',
          description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          instock: true,
          category: ['lace_frontal_wig']
        },
        {
          id: 63,
          size: [ '14', '20', '22', '24', '28'],
          price: 123.5,
          name:  'quibusdam natus ab reiciendis quia, cum laudantium tenetur alias maiores quisquam a.',
          description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          instock: false,
          category: ['lace_frontal_wig']
        },
        {
          id: 62,
          size: ['8', '10', '20', '22', '24'],
          price: 26.43,
          name:  'quibusdam natus ab reiciendis quia, cum laudantium tenetur alias maiores quisquam a.',
          description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          instock: true,
          category: ['lace_frontal_wig']
        },
        {
          id: 63,
          size: [ '14', '20', '22', '24', '28'],
          price: 123.5,
          name:  'quibusdam natus ab reiciendis quia, cum laudantium tenetur alias maiores quisquam a.',
          description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          instock: false
        },
        {
          id: 64,
          size: ['20', '22', '24'],
          price: 126.43,
          name:  'quibusdam natus ab reiciendis quia, cum laudantium tenetur alias maiores quisquam a.',
          description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          instock: true,
          category: ['full lace wig']
        },
        {
          id: 65,
          size: [ '14', '20','28'],
          price: 23.5,
          name:  'quibusdam natus ab reiciendis quia, cum laudantium tenetur alias maiores quisquam a.',
          description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Facilis',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          instock: true,
          category: ['full lace wig']
        },
      ]
      },
  bundle_deal:{
    id:3,
    routeName: 'bundle_deal',
    title: 'bundle deal',
    items: [
          {
          id:66,
          price: 43.89,
          texture:['straight', 'body wave'],
          lots: ' 3pcs / 4pcs wefts & 1 pcs cloure',
          name: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit',
          description: ' dicta quam. Neque praesentium atque perferendis. Quis tempora obcaecati vel explicabo placeat at architecto.',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          length: ['12, 24, 10', '24 20 10', '22 28 22'],
          grade: 'grade 8A',
          category: ['grade 8A']
      },
      {
          id:67,
          price: 453.89,
          texture:[ 'body wave', 'funmi hair'],
          lots: ' 2pcs / 2pcs wefts & 1 pcs cloure',
          name: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit',
          description: ' dicta quam. Neque praesentium atque perferendis. Quis tempora obcaecati vel explicabo placeat at architecto.',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          length: ['12, 24, 10', '24 20 10', '22 28 22'],
          grade: 'grade 8A',
          category: ['grade 8A']
      },
      {
          id:67,
          price: 43.89,
          texture:['straight', 'body wave'],
          lots: ' 3pcs / 4pcs wefts & 1 pcs cloure',
          name: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit',
          description: ' dicta quam. Neque praesentium atque perferendis. Quis tempora obcaecati vel explicabo placeat at architecto.',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          length: ['12, 24, 10', '24 20 10', '22 28 22'],
          grade: 'grade 9A',
          category: ['grade 9A']
      },
          {
          id:68,
          price: 453.89,
          texture:[ 'body wave', 'funmi hair'],
          lots: ' 2pcs / 2pcs wefts & 1 pcs cloure',
          name: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit',
          description: ' dicta quam. Neque praesentium atque perferendis. Quis tempora obcaecati vel explicabo placeat at architecto.',
          imageUrl: 'https://i.ibb.co/YTjW3vF/green-beanie.png',
          length: ['12, 24, 10', '24 20 10', '22 28 22'],
          grade: 'grade 9A',
          category: ['grade 9A']
      },
    ]
    }
  }

export default shop_data;