import {combineReducers} from 'redux'
import shopReducer from './shop/shopReducer';
import cartReducer from './cart-items/cart-reducer';
import {persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import {firebaseReducer} from 'react-redux-firebase'
import authReducer from './auth/authReducer'


const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['cart']
}
const rootReducer = combineReducers({
  auth: authReducer,
  shop: shopReducer,
  cart: cartReducer,
  firebase: firebaseReducer
})


export default persistReducer(persistConfig, rootReducer)