
import  cartTypes  from './cart-types';

export const toggleCartAction = () => ({
  type: cartTypes.TOGGLE_CART_HIDDEN
})

export const addCartItems = item => ({
  type: cartTypes.ADD_ITEMS,
  payload: item
})

export const removeCartItems = item => ({
  type: cartTypes.REMOVE_ITEMS,
  payload: item
})

export const clearItemsFromCart = (item) => ({
  type: cartTypes.CLEAR_ITEMS_FROM_CART,
  payload: item
})

export const sortingItems = (item) => ({
  type: cartTypes.SORT_ITEMS,
  payload: item
})