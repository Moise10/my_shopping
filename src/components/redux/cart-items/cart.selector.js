import { createSelector } from 'reselect';

const selectCart = state => state.cart;

export const selectCartHidden = createSelector(
  selectCart,
  cart => cart.hidden
)

export const selectCartCartItems = createSelector(
  selectCart,
  cart => cart.cartItems
)

export const selectSort = createSelector(
  selectCart,
  cart => cart.sort
)


export const selectCartQuantity = createSelector(
  selectCartCartItems,
  cartItems => cartItems.reduce((accumulatedValue, item) => accumulatedValue + item.quantity, 0)
)

export const selectCartTotal = createSelector(
  selectCartCartItems,
  cartItems => cartItems.reduce((accumulatedValue, item) => accumulatedValue + item.quantity * item.price, 0)
)