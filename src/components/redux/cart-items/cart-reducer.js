
import  cartTypes  from './cart-types';
import {addItemToCart, removeItemsFromCart , sortItems} from './cart.utils';

const INITIAL_STATE = {
  hidden: false,
  cartItems: [],
  sort: []
}

const cartReducer = (state = INITIAL_STATE, action) => {
  switch(action.type){
    case cartTypes.TOGGLE_CART_HIDDEN:
      return {
        ...state,
        hidden: !state.hidden
      };
    case cartTypes.ADD_ITEMS:
      return {
        ...state,
        cartItems: addItemToCart(state.cartItems, action.payload)
      };
    case cartTypes.CLEAR_ITEMS_FROM_CART:
      return{
        ...state,
        cartItems: [...state.cartItems.filter(cartItem => cartItem.id !== action.payload.id)]
      };
    case cartTypes.REMOVE_ITEMS:
      return{
        ...state,
        cartItems: removeItemsFromCart(state.cartItems, action.payload)
      };
    case cartTypes.SORT_ITEMS:
      return {
        ...state,
        sort: sortItems(state.sort, action.payload)
      }
    default:
      return state;
  }
}

export default cartReducer;