import React,{useState} from 'react';
import s from './bundles.scss'
import CssModules from 'react-css-modules'
import { Button } from 'reactstrap';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';


const Bundles = ({history}) => {
  const [dropdownOpen, setOpen] = useState(false);

  const toggle = () => setOpen(!dropdownOpen);
  return ( 
    <div>
    <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
          <Button id="caret"  className='Button' style={{color:'black', fontSize: '19px'}}
          onClick={ () => history.push('/collection/bundle_deal')}>
          Bundles
          </Button>
          <DropdownToggle caret color="black" />
          <DropdownMenu>
          <DropdownItem >
              <Link to='/collection/grade_8a'>Grade 8A + hair weft/ lace</Link>
            </DropdownItem>
            <DropdownItem onClick={() => history.push('/collection/grade_9a')}>Grade 9A hair weft / lace closure 
            </DropdownItem>
          </DropdownMenu>
      </ButtonDropdown>
    </div>
  );
}

export default withRouter(CssModules(Bundles,s));