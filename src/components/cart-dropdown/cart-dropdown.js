import React from 'react'
import s from './cart-dropdown.scss'
import CssModules from 'react-css-modules'
import CustomButton from '../custom-button/custom-button'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {selectCartCartItems} from '../redux/cart-items/cart.selector'
import CartItem from '../cart-item/cart-item'
import { withRouter } from 'react-router-dom';
import { toggleCartAction } from '../redux/cart-items/cart-Actions';


const CartDropdown = ({cartItems, history,dispatch}) => {
  return ( 
    <div className="cart-dropdown">
      <div className="cart-items">
        
      {
        cartItems.length === 0 ? <p className='empty-message'>Your cart is empty </p> :
          cartItems.map(cartItem => {
            return <CartItem  key={cartItem.id} item={cartItem}/>
          })
        }
      </div>
      <CustomButton onClick={() => {
        history.push('/checkout')
        dispatch(toggleCartAction())
        }}>CHECKOUT</CustomButton>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  cartItems: selectCartCartItems
})
export default connect(mapStateToProps)(withRouter(CssModules(CartDropdown,s)));