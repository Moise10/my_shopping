import React from 'react';
import image from '../../../assets/images/image2.jpg'
import s from './homepage.scss'
import CssModules from 'react-css-modules'
import CustomButton from '../../custom-button/custom-button'
import Directory from '../../directory/directory';
import { withRouter } from 'react-router-dom';

const Homepage = ({history}) => {
  return ( 
    <div>
      <div className='directory'>
      <Directory />
      </div>
      
    <div className='homepage'>
      <div className="image" 
      style={{backgroundImage: `url(${image})`}}
      />
      <div className="message">
        <span className='text'>Sparkle your hair style</span>
        <CustomButton onClick={() => history.push('/collection/virgin_hair')}>SHOP NOW</CustomButton>
      </div>
    </div>
    </div>
  );
}

export default withRouter(CssModules(Homepage,s));