import React from 'react';
import s from './contact-page.scss'
import CssModules from 'react-css-modules'
import Directory from '../../directory/directory'



const ContactPage = () => {
  return ( 
    <div>
      <div className="directory">
        <Directory />
      </div>
      Contact page
    </div>
   );
}
 
export default ContactPage;