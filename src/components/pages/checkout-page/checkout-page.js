import React from 'react'
import s from './checkout-page.scss'
import CssModules from 'react-css-modules'
import { connect } from 'react-redux';
import { selectCartCartItems, selectCartTotal } from '../../redux/cart-items/cart.selector';
import { createStructuredSelector } from 'reselect';
import CheckoutItem from '../../checkout-items/checkout-items';
import Directory from '../../directory/directory';

const CheckoutPage = ({cartItems, total}) => {
  return ( 
    <div>
    <div className='directory'>
      <Directory />
      </div>
      <div className='checkout-page'>
        <div className="checkout-header">
          <div className="headerBlock">
            <span>Product</span>
          </div>
          <div className="headerBlock">
            <span>Description</span>
          </div>
          <div className="headerBlock">
          <span>Quantity</span>
          </div>
          <div className="headerBlock">
            <span>Price</span>
          </div>
          <div className="headerBlock">
            <span>Remove</span>
          </div>
        </div>
        {
          cartItems.map(cartItem => {
            return <CheckoutItem  key={cartItem.id} cartItem={cartItem}/>
          })
        }
        <div className="total">
          <span>TOTAL: ${total}</span>
          <div>CHECKOUT</div>
        </div>
        </div>
      </div>
  );
}

const mapStateToProps = createStructuredSelector({
  cartItems: selectCartCartItems,
  total: selectCartTotal
})

export default connect(mapStateToProps)(CssModules(CheckoutPage,s));