import React from 'react'
import  classes from './sign-up.module.scss';
import CustomButton from '../custom-button/custom-button'
import CustomInput from '../custom-input/custom-input';
import { signUp } from '../redux/auth/authActions';
import { connect } from 'react-redux';


class SignUp extends React.Component {
  state = { 
    email: '',
    name: '',
    password: '',
    confirmPassword: ''
  }

  handleChange = (event) => {
    const {name, value} = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.signUp(this.state)
  }

  render() { 
    const {name, email, password, confirmPassword} = this.state;
    return ( 
      <div className={classes.sign_up}>
        <h2>I don't have an account </h2>
        <p className={classes.title}>Sign up with your name and email</p>
        <form action="" onSubmit={this.handleSubmit}>

          <CustomInput required label='name' type='text' 
          value= {name} name='name'
          handleChange={this.handleChange}
          />
          <CustomInput label='email' required value={email} name='email' type='email' handleChange={this.handleChange} />

          <CustomInput label='password' required value={password} name='password' type='password' handleChange={this.handleChange} />

          <CustomInput label='confirmPassword' required value={confirmPassword} name='confirmPassword' type='password' handleChange={this.handleChange} />

          <CustomButton >SIGN UP</CustomButton>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(state)
  return {

  }
}

const mapDispatchToProps = dispatch => {
  return {
    signUp: newUser => dispatch(signUp(newUser))
  }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);