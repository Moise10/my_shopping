import React from 'react'
import s from './collection_page_overview.scss'
import CssModules from 'react-css-modules'
import CollectionItem from '../collection-items/collection-item'
import { connect } from 'react-redux';
import {selectCollection} from '../redux/shop/shopSelector';
import Directory from '../directory/directory'
import { Link } from 'react-router-dom';



const CollectionPageOverview = ({collection, match}) => {
  const { products}  = collection;
  console.log(products, match)
  const arr =  Object.values(products)
  return ( 
  <div>
    <Directory />
    <input type="search" placeholder='search products'/>

    <div className='collection-page'>
      <div className='items'>
      { arr.map(product => {
        return (
          <Link key={product.id} to={match.url+'/'+product.id}>
          <CollectionItem  item={product} />
          </Link>
        )
      })}
      </div>
    </div>
  </div>
  );
}

const mapStateToProps = (state, ownProps) => ({
  collection: selectCollection(ownProps.match.params.collectionId)(state)
})

export default connect(mapStateToProps)(CssModules(CollectionPageOverview,s));