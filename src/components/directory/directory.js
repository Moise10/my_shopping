import React from 'react';
import s from './directory.scss'
import CssModules from 'react-css-modules'
import {Link, withRouter} from 'react-router-dom'
import Wigs from '../wig-collection/wig';
import VirginHair from '../hair-collections/virgin-hair';
import Bundles from '../bundle-collections/bundles';
import CartIcon from '../cart-icon/cart-icon';
import HeartIcon from '../heart-icon/heart-icon';
import CartDropdown from '../cart-dropdown/cart-dropdown'
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { selectCartHidden } from '../redux/cart-items/cart.selector';
import {signOutAuth} from '../redux/auth/authActions';
import { uid } from '../redux/auth/authSelector';



const Directory = ({ hidden, signOut, auth}) => {
  const links = auth ? 
  <div className='option' onClick={signOut}>SIGN OUT</div> : 
  <Link to='/signin' className='option'>SIGN IN</Link>
  return ( 
    <div>
    <div className='directory'>
      <div className="logo-text">
        Joy's.Beauty accessories
      </div>
      <div className="options">
        <Link to='/' className='option'>HOME</Link>
        <Link to='/contact' className='option'>CONTACT</Link>
        {links}
        <CartIcon />
        <HeartIcon />
      </div>
      {
        hidden ?
      <CartDropdown />
      : null
      }
      </div>

      <div className='collections'>
      <div className='collection-title'>
          <Link  to='/accessories' className='title'>accessories</Link>
          <div className='title'><VirginHair /></div>
          <div className='title'><Bundles /></div>
          <div className='title'><Wigs /></div>
          <Link  to='/collection_page' className='title'>blog</Link>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  hidden: selectCartHidden,
  auth: uid
})

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOutAuth())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CssModules(Directory,s)));