import React,{useState} from 'react';
import s from './virgin-hair.scss'
import CssModules from 'react-css-modules'
import { Button } from 'reactstrap';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { withRouter} from 'react-router-dom';


const VirginHair = ({history}) => {
  const [dropdownOpen, setOpen] = useState(false);

  const toggle = () => setOpen(!dropdownOpen);

  return ( 
    <div> 
    <div>
        <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
          <Button id="caret"  className='Button' style={{color:'black', fontSize: '19px'}}
          onClick={ () => history.push('/collection/virgin_hair')}>
          Virgin hair
          </Button>
          <DropdownToggle caret color="black" />
          <DropdownMenu>
            <DropdownItem onClick={ () => history.push('/collection/best_selling')}>
              Best Selling
            </DropdownItem>
            <DropdownItem onClick={ () => history.push('/collection/hair_bundles')}>
              Hair bundles
            </DropdownItem>
            <DropdownItem onClick={ () => history.push('/collection/closure_frontal')}>
            closure / frontal
            </DropdownItem>
            <DropdownItem onClick={ () => history.push('/collection/blonde_hair_with_closure')}>
            613 blonde hair
            </DropdownItem>
            <DropdownItem onClick={ () => history.push('/collection/ombre_hair')}>
            Ombre hair
            </DropdownItem>
          </DropdownMenu>
      </ButtonDropdown>
      </div>
      
    </div>
  );
}



export default withRouter(CssModules(VirginHair,s));