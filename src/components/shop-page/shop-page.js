import React from 'react';
import { Switch, Route} from 'react-router-dom';
import CollectionOverview from '../collection-overview/collection-overview';
import CollectionPage from '../collection-page/collection-page';
import { connect } from 'react-redux';
import { fetchCollectionsAsync } from '../redux/shop/shopAction';
import WithSpinner from '../with-spinner/with-spinner';
import { createStructuredSelector } from 'reselect';
import { selectCollectionIsFetching, selectIsCollectionLoaded } from '../redux/shop/shopSelector';
import Product from '../product/product';


const CollectionOverviewWithSpinner = WithSpinner(CollectionOverview);
const CollectionPageWithSpinner = WithSpinner(CollectionPage);
const productWithRouterSpinner = WithSpinner(Product)


class ShopPage extends React.Component {

  componentDidMount(){
    const {fetchCollectionsAsync} = this.props;
    fetchCollectionsAsync();
  }
  render() { 
    const {isCollectionsfetching, isCollectionsLoaded} = this.props;
    return ( 
      <div>
        <Switch>
          <Route exact path={`${this.props.match.path}`} render={(props) => <CollectionOverviewWithSpinner 
          isLoading={isCollectionsfetching} {...props}/>}/>

          <Route path={`${this.props.match.path}/:collectionId`} 
          render={(props) => <CollectionPageWithSpinner 
          isLoading={!isCollectionsLoaded} {...props}/>}/>
        </Switch>
      </div>
    );
  }
}

const mapDispatchtoProps = dispatch => ({
  fetchCollectionsAsync: () => dispatch(fetchCollectionsAsync())
})

const mapStateToProps = createStructuredSelector({
isCollectionsfetching: selectCollectionIsFetching,
isCollectionsLoaded: selectIsCollectionLoaded
})

export default connect(mapStateToProps, mapDispatchtoProps)(ShopPage);