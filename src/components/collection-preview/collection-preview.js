import React from 'react';
import s from './collection-preview.scss'
import CssModules from 'react-css-modules'
import CollectionItem from '../collection-items/collection-item';
import { withRouter } from 'react-router-dom';
import {Link} from 'react-router-dom'
import product from '../product/product';

const CollectionPreview = ({title, products, history, linkUrl, match}) => {
  console.log(match)
  return ( 
    <div className="collection-preview">
      <h2 className='title' onClick={() => history.push(`${match.url}${linkUrl}`)}>{title}</h2>
      <div className="preview">
        {
          products ? Object.values(products).filter((id, idx) => idx < 4).map(item => {
            return (
            <Link to={'/product/' + product.id} key={item.id}>
            <CollectionItem  item={item} />
            </Link>
          )})
          
        : <div>Loading...</div>}
      </div>
    </div>
    );
}

export default withRouter(CssModules(CollectionPreview,s));