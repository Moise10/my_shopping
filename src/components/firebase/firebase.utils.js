import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = ({
  apiKey: "AIzaSyCRlOLjry4IiPeH5G5uLdpfh0myGx72nkU",
  authDomain: "joy-s-6f840.firebaseapp.com",
  databaseURL: "https://joy-s-6f840.firebaseio.com",
  projectId: "joy-s-6f840",
  storageBucket: "joy-s-6f840.appspot.com",
  messagingSenderId: "401576519935",
  appId: "1:401576519935:web:deb7273e971b4fa891c4ca",
  measurementId: "G-GGXLYQ7ESW"

})
firebase.initializeApp(config)

export const auth = firebase.auth();
export const firestore = firebase.firestore();



export const addCollectionsAndDocuments = async (collectionKey, objectToAdd) => {

  const collectionRef = firestore.collection(collectionKey);
  const batch = firestore.batch();
  objectToAdd.forEach(obj => {
    const newDocRef = collectionRef.doc()
    batch.set(newDocRef, obj)
  })
  return await batch.commit()
}


const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({
  'prompt': 'select_account'
});

export const signInWithGoogle = () => auth.signInWithPopup(provider)

export default firebase;