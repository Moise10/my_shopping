import React from 'react';
import s from './checkout-items.scss'
import CssModules from 'react-css-modules'
import { connect } from 'react-redux';
import { clearItemsFromCart } from '../redux/cart-items/cart-Actions';
import {addCartItems , removeCartItems} from '../redux/cart-items/cart-Actions'


const CheckoutItem = ({cartItem, dispatch}) => {
  const {imageUrl, name, price, quantity} = cartItem
  return ( 
    <div className="checkout-item">
      <div className="image-container">
        <img src={imageUrl} alt=""/>
      </div>
      <span className="name">{name}</span>
      <span className="quantity">
      <div className="arrow" onClick={() => dispatch(removeCartItems(cartItem))}>&#10094;</div>
        <span className="value">{quantity}</span>
      <div className="arrow" onClick={() => dispatch(addCartItems(cartItem))}>&#10095;</div>
        </span>
      <span className="price">{price}</span>
      <div className="remove-button" onClick={() => dispatch(clearItemsFromCart(cartItem))}>&#10005;</div>
    </div>
  );
}

export default connect()(CssModules(CheckoutItem,s));