import React from 'react'
import classes from './sign-in.module.scss'
import CustomInput from '../custom-input/custom-input'
import CustomButton from '../custom-button/custom-button'
import {  signInWithGoogle} from '../firebase/firebase.utils';
import { signInAuth} from '../redux/auth/authActions';
import { connect } from 'react-redux';



class SignIn extends React.Component {
  state = { 
    password: '',
    email: ''
    }

    
    handleSubmit = async (event) => {
      event.preventDefault();
      this.props.signIn(this.state)
    }

    handleChange = (event) => {
      const {value, name} = event.target;
      this.setState({ [name]: value });
    }

  render() { 
    const {email, password} = this.state
    return ( 
      <div className={classes.sign_in}>
        <h2>I already have an account </h2>
        <p className={classes.title}>Sign in with your email and password</p>
        <form action="" onSubmit={this.handleSubmit}>
          <CustomInput 
          type='email'
          name='email'
          handleChange={this.handleChange}
          label='email'
          required
          value={email}
          />
          <CustomInput 
          type='password'
          name='password'
          handleChange={this.handleChange}
          label='password'
          required
          value={password}
          />
          <div className={classes.buttons}>
          <CustomButton type='submit'>SIGN IN</CustomButton>
          <CustomButton onClick={signInWithGoogle}
          isGoogleSignIn>SIGN IN WITH GOOGLE</CustomButton>
          </div>
          
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (credentials) => dispatch(signInAuth(credentials)),
    
  }
}


export default connect(null, mapDispatchToProps)(SignIn);