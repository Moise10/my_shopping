import React from 'react';
import s from './collection-item.scss'
import CssModules from 'react-css-modules'
import CustomButton from '../custom-button/custom-button'
import HeartIcon from '../heart-icon/heart-icon';
import { connect } from 'react-redux';
import { addCartItems } from '../redux/cart-items/cart-Actions'
import { withRouter } from 'react-router-dom';

const CollectionItem = ({item, addItems, history}) => {
  const {imageUrl, price, name} = item
  return ( 
    <div className="collection-item"
    >
    <div className="image" 
      style={{backgroundImage: `url(${imageUrl})`}}/>
      <div className='name'>
      <span>{name}</span>
      </div>
      <div className="collection-footer">
        <span className="price">${price}</span>
        <div  className='heart' onClick={() => addItems(item)} ><HeartIcon /></div>
      </div>
      <CustomButton onClick={() => addItems(item)} inverted='true' >ADD TO CART</CustomButton>
    </div>
    );
}

const mapDispatchToProps = dispatch => ({
  addItems: item => dispatch(addCartItems(item))
})

export default connect(null, mapDispatchToProps)(withRouter(CssModules(CollectionItem,s)));