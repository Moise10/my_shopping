import React from 'react'
import s from './heart-icon.scss'
import CssModules from 'react-css-modules'
import heart from '../../assets/icons/heart.svg'


const HeartIcon = () => {
  return ( 
    <div >
      <img src={heart} alt="" className='heart-icon'/>
    </div>
    );
}

export default CssModules(HeartIcon,s);