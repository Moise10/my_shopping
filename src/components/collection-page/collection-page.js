import React from 'react';
import CollectionPageOverview from '../collectionPage-overview/collection_page_overview';
import { Route } from 'react-router-dom';
import Product from '../product/product';
import WithSpinner from '../with-spinner/with-spinner';
import {fetchCollectionsAsync }from '../redux/shop/shopAction';
import {selectCollectionIsFetching, selectIsCollectionLoaded} from '../redux/shop/shopSelector'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';


const CollectionPageOverviewWithSpinner = WithSpinner(CollectionPageOverview);
const ProductWithSpinner = WithSpinner(Product)

class CollectionPage extends React.Component {

  componentDidMount(){
    const {fetchCollectionsAsync} = this.props;
    fetchCollectionsAsync()
  }

  render() { 
    const {isCollectionFetching, isCollectionLoaded} = this.props
    return ( 
      <div>
      <Route exact path={`${this.props.match.path}`} render={(props) => <CollectionPageOverviewWithSpinner isLoading={!isCollectionLoaded}
      {...props}/>}/>
      <Route path={`${this.props.match.url}/:id`} render={(props) => 
      <ProductWithSpinner isLoading={!isCollectionLoaded} {...props}/>}/>
    </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchCollectionsAsync: () => dispatch(fetchCollectionsAsync)
})

const mapStatetoProps = createStructuredSelector({
  isCollectionFetching: selectCollectionIsFetching,
  isCollectionLoaded: selectIsCollectionLoaded
})

export default connect(mapStatetoProps, mapDispatchToProps)(CollectionPage);