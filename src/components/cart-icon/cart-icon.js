import React from 'react'
import s from './cart-icon.scss'
import CssModules from 'react-css-modules'
import {ReactComponent as ShoppingBag} from '../../assets/icons/shopping-bag.svg';
import  {toggleCartAction}  from '../redux/cart-items/cart-Actions';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectCartQuantity } from '../redux/cart-items/cart.selector';

const CartIcon = ({toggleCart, itemCount}) => {
  return ( 
    <div className='cart-icon' onClick={toggleCart}>
      <ShoppingBag className='shopping-icon'/>
      <span className='item-count'>{itemCount}</span>
    </div>
  );
}

const mapDispatchToProps = dispatch => ({
  toggleCart: () => dispatch(toggleCartAction())
})

const mapStateToProps = createStructuredSelector({
  itemCount: selectCartQuantity
})


export default connect(mapStateToProps, mapDispatchToProps)(CssModules(CartIcon,s));