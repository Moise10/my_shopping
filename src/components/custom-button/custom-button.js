import React from 'react';
import s from './custom-button.scss'
import CssModules from 'react-css-modules'


const CustonButton = ({children,isGoogleSignIn, ...otherProps}) => {
  return ( 
    <button className={`${isGoogleSignIn ? 'google-sign-in' : ''} custom-button`} {...otherProps}
    >{children}
    </button>
  );
}

export default CssModules(CustonButton,s);