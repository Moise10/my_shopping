import React from 'react'
import s from './collection-overview.scss'
import CssModules from 'react-css-modules'
import CollectionPreview from '../collection-preview/collection-preview'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { convertCollectionsToArray, selectProducts} from '../redux/shop/shopSelector';
import Directory from '../directory/directory';


const CollectionOverview = ({collection, products}) => {
  console.log(Object.values(products));
  return ( 
    <div>
      <Directory />
    <div className='collection'>
      {
        collection.map(({id, ...otherCollectionProps}) => {
          return <CollectionPreview key={id} {...otherCollectionProps} />
        })
      }
    </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  collection: convertCollectionsToArray,
  products: selectProducts
})

export default 
  connect(mapStateToProps)(CssModules(CollectionOverview,s));