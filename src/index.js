import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter} from 'react-router-dom'
import {Provider} from 'react-redux'
// import {Store, persistor} from './components/redux/store/store';
import { PersistGate } from 'redux-persist/integration/react';
import {createStore, applyMiddleware, compose} from 'redux'
import logger from 'redux-logger'
import rootReducer from '../src/components/redux/rootReducer';
import {persistStore} from 'redux-persist';
import thunk from 'redux-thunk';
import {getFirestore, reduxFirestore, createFirestoreInstance} from 'redux-firestore';
import {getFirebase, ReactReduxFirebaseProvider} from 'react-redux-firebase';
import fbConfig from '../src/components/firebase/firebase.utils';
import firebase  from 'firebase/app'

const middleware = []

if(process.env.NODE_ENV === "development"){
  middleware.push(logger)
}
const store = createStore(rootReducer, 
  compose(
  applyMiddleware(thunk.withExtraArgument({
  getFirebase , getFirestore})),
  reduxFirestore(fbConfig)
  )
  )
  const persistor = persistStore(store)

  const rrfProps = {
    firebase,
    config: fbConfig,
    dispatch: store.dispatch,
    createFirestoreInstance
  };

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <PersistGate  persistor={persistor}>
        <ReactReduxFirebaseProvider {...rrfProps}>
      <App />
        </ReactReduxFirebaseProvider>
      </PersistGate>
    </BrowserRouter>
  </Provider>
  ,
  document.getElementById('root')
);

