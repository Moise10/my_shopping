import React from 'react';
import Homepage from './components/pages/homepage/homepage';
import {Route, Switch, Redirect} from 'react-router-dom'
import './App.scss'
import ShopPage from '../src/components/shop-page/shop-page'
import SignInSignUp from './components/sign-in-sign-up/sign-in-sign-up';
import CheckoutPage from './components/pages/checkout-page/checkout-page';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import ContactPage from './components/pages/contact-page/contact-page';
import Product from './components/product/product';
import { convertCollectionsToArray, selectCollection} from './components/redux/shop/shopSelector';
import { uid } from './components/redux/auth/authSelector';
import {selectCollections} from './components/redux/shop/shopSelector'
import { addCollectionsAndDocuments } from './components/firebase/firebase.utils';
import WithSpinner from './components/with-spinner/with-spinner';
import { fetchCollectionsAsync } from './components/redux/shop/shopAction';
import {selectCollectionIsFetching, selectIsCollectionLoaded} from './components/redux/shop/shopSelector'


const ProductWithSpinner = WithSpinner(Product)

class App extends React.Component {

  componentDidMount(){
    const {fetchCollectionsAsync} = this.props;
    fetchCollectionsAsync();
  }

  // unSubscribedFromAuth = null;

  // componentDidMount(){
  //   //adding collections
      
  // const { collectionArray} = this.props;
  //   this.unSubscribedFromAuth = addCollectionsAndDocuments('collections', collectionArray.map(({title, products}) => ({title, products})))
  // }

  // componentWillUnmount(){
  //   this.unSubscribedFromAuth()
  // }

  render() {
    const {collections, isCollectionsLoaded,isCollectionsfetching} = this.props;
    console.log(collections)
    return ( 
    <div className='app'>
      <Switch>
        <Route exact path='/' component={Homepage}/>
        <Route path='/collection' component={ShopPage} />
        <Route path='/checkout' component={CheckoutPage} />
        <Route path='/signin' render={() => this.props.auth ? 
          <Redirect to='/'/> : <SignInSignUp /> }/>
        <Route path='/contact' component={ContactPage}/>
      </Switch>
    </div> 
  );
  }
}

const mapStateToProps = createStructuredSelector({
  collectionArray: convertCollectionsToArray,
  auth: uid,
  collections: selectCollections,
  isCollectionsfetching: selectCollectionIsFetching,
  isCollectionsLoaded: selectIsCollectionLoaded
})

const mapDispatchtoProps = dispatch => ({
  fetchCollectionsAsync: () => dispatch(fetchCollectionsAsync())
})

// state => ({
//   collections: state.shop.collections
// })

export default connect(mapStateToProps, mapDispatchtoProps)(App);